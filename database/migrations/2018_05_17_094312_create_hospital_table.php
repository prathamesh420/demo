<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHospitalTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hospital', function (Blueprint $table) {
            $table->increments('id');
            $table->text('pattern');
            $table->text('sleeve');

            $table->text('logoPlacement');
            $table->text('text');
            $table->text('textPlacement');
            $table->text('color');


            $table->integer('small');
            $table->integer('medium');
            $table->integer('large');
            $table->integer('xl');
            $table->integer('xxl');
            $table->integer('xxxl');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hospital');
    }
}
