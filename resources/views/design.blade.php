@extends('layouts.app')



@section('content')
<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link" id="home-tab" data-toggle="tab" href="#event" role="tab" aria-controls="home" aria-selected="true">Bulk Tshirts for Events</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#hospital" role="tab" aria-controls="profile" aria-selected="false">Bulk Uniforms for Hospitality Industry</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#hiviz" role="tab" aria-controls="contact" aria-selected="false">Bulk Hiviz Tshirts & Worke Wear</a>
  </li>
</ul>


<!--Bilk tshirts for Events-->
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="event" role="tabpanel" aria-labelledby="event-tab"><br>
    <form method="post" action= " {{ url('/design/insert') }} ">
    {{ csrf_field() }}
        <div class="container" >
        <label class="control-label" for="pattern">Select Pattern :</label>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">    
                        <select class="form-control selectpicker" id="pattern" name="pattern">
                            <option>None</option>
                            <option value="roundNeck">Round Neck</option>
                            <option value="vNeck">V Neck</option>
                            <option value="collarNeck">Collar Neck</option>
                        </select>
                    </div>
                </div>

              
                <div class="col-md-6">
                    <label class="radio-inline">&nbsp;
                        <input type="radio" name="sleeve" id="halfSleeve" value="halfSleeve">&nbsp;&nbsp;Half Sleeve
                    </label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <label class="radio-inline">
                        <input type="radio" name="sleeve" id="fullSleeve" value="fullSleeve">&nbsp;&nbsp;Full Sleeve
                    </label> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="image">Upload Your Image :</label>
                        <input type="file" class="form-control file" id="image" name="image"/>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="logoPlacement">Logo Placement :</label>
                            <select class="form-control selectpicker" id="logoPlacement" name="logoPlacement">
                            <option>None</option>
                                <option value="rightChest">Right Chest</option>
                                <option value="leftChest">Left Chest</option>
                                <option value="shoulders">Shoulders</option>
                            </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <label class="control-label" for="textPlacement">Enter Text you want to Print !</label>
                    <div class="md-form">
                        <input type="text" id="text" name="text" class="form-control" placeholder="Enter text you want to print !">
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="textPlacement">Text Placement :</label>
                        <select class="form-control selectpicker" id="textPlacement" name="textPlacement">
                        <option>None</option>
                        <option value="front">Front Side</option>
                        <option value="back">Back Side</option>
                        <option value="side">Side by Side</option>
                        </select>
                    </div>
                </div>  
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="color">Select Color :</label>
                        <select class="form-control selectpicker" id="color" name="color">
                            <option>None</option>
                            <option value="Red">Red</option>
                            <option value="Blue">Blue</option>
                            <option value="Green">Green</option>
                            <option value="White">White</option>
                            <option value="Black">Black</option>
                            <option value="Grey">Grey</option>
                            <option value="Yellow">Yellow</option>
                            <option value="Brown">Brown</option>
                        </select>
                    </div>
                </div>
            </div>
            

            <label class="control-label" for="quantity">Enter Quantity as per size:</label>
            <div class="row">
                <div class="col-md-6">  
                    <div class="row">
                        <div class="col-md-1">
                            <h3><span class="badge blue">S</span></h3>
                        </div>
                        <div class="col-md-3">
                            <div class="md-form">
                                <input type="number" id="small" name="small" class="form-control">
                                <label class="active" for="small">Quantity</label>
                            </div>
                        </div>
                    </div>
                
                    <div class="row">
                        <div class="col-md-1">
                            <h3><span class="badge blue">M</span></h3>
                        </div>
                        <div class="col-md-3">
                            <div class="md-form">
                                <input type="number" id="medium" name="medium" class="form-control">
                                <label class="active" for="medium" >Quantity</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1">
                            <h3><span class="badge blue">L</span></h3>
                        </div>
                        <div class="col-md-3">
                            <div class="md-form">
                                <input type="number" id="large" name="large" class="form-control">
                                <label class="active" for="large" >Quantity</label>
                            </div>
                        </div>
                    </div>
                </div>
         
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-2">
                            <h3><span class="badge blue">XL</span></h3>
                        </div>
                        <div class="col-md-3">
                            <div class="md-form">
                                <input type="text" id="xl" name="xl" class="form-control">
                                <label class="active" for="xl" >Quantity</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <h3><span class="badge blue">XXL</span></h3>
                        </div>
                        <div class="col-md-3">
                            <div class="md-form">
                                <input type="text" id="xxl" name="xxl" class="form-control">
                                <label class="active" for="xxl" >Quantity</label>
                            </div>
                        </div>
                    </div>
                
                    <div class="row">
                        <div class="col-md-2">
                            <h3><span class="badge blue">XXXL</span></h3>
                        </div>
                        <div class="col-md-3">
                            <div class="md-form">
                                <input type="text" id="xxxl" name="xxxl" class="form-control">
                                <label class="active" for="xxxl" >Quantity</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div><br>
            <button type="submit" class="btn btn-primary">Ask For Quote</button>
        </div>
    </form> 
</div>



    <!--Bulk Uniforms-->
    <div class="tab-pane fade" id="hospital" role="tabpanel" aria-labelledby="profile-tab"><br>
    <form method="post">
        <div class="container">
        <label class="control-label" for="pattern">Select Pattern :</label>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        
                        <select class="form-control selectpicker" id="pattern" name="pattern">
                            <option value="none">None</option>
                            <option value="roundNeck">Round Neck</option>
                            <option value="vNeck">V Neck</option>
                            <option value="collarNeck">Collar Neck</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <label class="radio-inline">
                        <input type="radio" name="sleeve" id="halfSleeve" value="option1">Half Sleeve
                    </label>&nbsp;&nbsp;&nbsp;
                    <label class="radio-inline">
                        <input type="radio" name="sleeve" id="fullSleeve" value="option2">Full Sleeve
                    </label> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="image">Upload Your Image :</label>
                        <input type="file" class="form-control-file" id="image">
                        <button type="button" class="btn btn-primary">Upload</button>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="logoPlacement">Logo Placement :</label>
                        <select class="form-control selectpicker" id="logoPlacement" name="logoPlacement">
                        <option>None</option>
                        <option value="rightChest">Right Chest</option>
                        <option value="leftChest">Left Chest</option>
                        <option value="shoulders">Shoulders</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="text">Enter text you want to print !</label>
                        <input type="text" class="form-control" id="text" name="text" >
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="textPlacement">Text Placement :</label>
                        <select class="form-control selectpicker" id="textPlacement" name="textPlacement">
                        <option>None</option>
                        <option value="front">Front Side</option>
                        <option value="back">Back Side</option>
                        <option value="side">Side by Side</option>
                        </select>
                    </div>
                </div>  
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="color">Select Color :</label>
                        <select class="form-control selectpicker" id="color" name="color">
                            <option>None</option>
                            <option value="1">Red</option>
                            <option value="2">Blue</option>
                            <option value="3">Green</option>
                            <option value="4">White</option>
                            <option value="5">Black</option>
                            <option value="6">Grey</option>
                            <option value="7">Yellow</option>
                            <option value="8">Brown</option>
                        </select>
                    </div>
                </div>
            </div>
            

            <label  class="control-label" for="quantity">Enter Quantity as per size:</label>
            <div class="row">
                <div class="col-md-6">  
                    <div class="row">
                        <div class="col-md-1">
                            <h3><span class="badge blue">S</span></h3>
                        </div>
                        <div class="col-md-3">
                            <div class="md-form">
                                <input type="number" id="small" name="small" class="form-control">
                                <label class="active" for="small" >Quantity</label>
                            </div>
                        </div>
                    </div>
                
                    <div class="row">
                        <div class="col-md-1">
                            <h3><span class="badge blue">M</span></h3>
                        </div>
                        <div class="col-md-3">
                            <div class="md-form">
                                <input type="number" id="medium" name="medium" class="form-control">
                                <label class="active" for="medium" >Quantity</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1">
                            <h3><span class="badge blue">L</span></h3>
                        </div>
                        <div class="col-md-3">
                            <div class="md-form">
                                <input type="number" id="large" name="large" class="form-control">
                                <label class="active" for="large" >Quantity</label>
                            </div>
                        </div>
                    </div>
                </div>
         

                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-2">
                            <h3><span class="badge blue">XL</span></h3>
                        </div>
                        <div class="col-md-3">
                            <div class="md-form">
                                <input type="number" id="xl" name="xl" class="form-control">
                                <label class="active" for="xl" >Quantity</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <h3><span class="badge blue">XXL</span></h3>
                        </div>
                        <div class="col-md-3">
                            <div class="md-form">
                                <input type="number" id="xxl" name="xxl" class="form-control">
                                <label class="active" for="xxl" >Quantity</label>
                            </div>
                        </div>
                    </div>
                
                    <div class="row">
                        <div class="col-md-2">
                            <h3><span class="badge blue">XXXL</span></h3>
                        </div>
                        <div class="col-md-3">
                            <div class="md-form">
                                <input type="number" id="xxxl" name="xxxl" class="form-control">
                                <label class="active" for="xxxl" >Quantity</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div><br>
            <button type="submit" class="btn btn-primary">Ask For Quote</button>
        </div>
    </form> 


  </div>

  <!--Bulk Hiviz-->
  <div class="tab-pane fade" id="hiviz" role="tabpanel" aria-labelledby="contact-tab"><br>

 <form method="post">
        <div class="container">
        <label  class="control-label" for="pattern">Select Pattern :</label>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        
                        <select class="form-control selectpicker" id="pattern" name="pattern">
                            <option>None</option>
                            <option value="roundNeck">Round Neck</option>
                            <option value="vNeck">V Neck</option>
                            <option value="collarNeck">Collar Neck</option>
                        </select>
                    </div>
                </div>

                <div class="col-md-6">
                    <label class="radio-inline">
                        <input type="radio" name="sleeve" id="halfSleeve" value="option1">Half Sleeve
                    </label>&nbsp;&nbsp;&nbsp;
                    <label class="radio-inline">
                        <input type="radio" name="sleeve" id="fullSleeve" value="option2">Full Sleeve
                    </label> 
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label  class="control-label" for="image">Upload Your Image :</label>
                        <input type="file" class="form-control-file" id="image">
                        <button type="button" class="btn btn-primary">Upload</button>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="logoPlacement" name="logoPlacement">Logo Placement :</label>
                        <select class="form-control selectpicker" id="logoPlacement">
                        <option>None</option>
                        <option value="rightChest">Right Chest</option>
                        <option value="leftChest">Left Chest</option>
                        <option value="shoulders">Shoulders</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="text">Enter text you want to print !</label>
                        <input type="text" class="form-control" id="text" name="text" >
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label" for="textPlacement">Text Placement :</label>
                        <select class="form-control selectpicker" id="textPlacement" name="textPlacement">
                        <option>None</option>
                        <option value="front">Front Side</option>
                        <option value="back">Back Side</option>
                        <option value="side">Side by Side</option>
                        </select>
                    </div>
                </div>  
            </div>

            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label  class="control-label" for="color">Select Color :</label>
                        <select class="form-control selectpicker" id="color" name="color">
                            <option>None</option>
                            <option value="1">Red</option>
                            <option value="2">Blue</option>
                            <option value="3">Green</option>
                            <option value="4">White</option>
                            <option value="5">Black</option>
                            <option value="6">Grey</option>
                            <option value="7">Yellow</option>
                            <option value="8">Brown</option>
                        </select>
                    </div>
                </div>
            </div>
            

            <label class="control-label" for="pattern">Enter Quantity as per size:</label>
            <div class="row">
                <div class="col-md-6">  
                    <div class="row">
                        <div class="col-md-1">
                            <h3><span class="badge blue">S</span></h3>
                        </div>
                        <div class="col-md-3">
                            <div class="md-form">
                                <input type="number" id="small" name="small" class="form-control">
                                <label class="active" for="small" >Quantity</label>
                            </div>
                        </div>
                    </div>
                
                    <div class="row">
                        <div class="col-md-1">
                            <h3><span class="badge blue">M</span></h3>
                        </div>
                        <div class="col-md-3">
                            <div class="md-form">
                                <input type="number" id="medium" name="medium" class="form-control">
                                <label class="active" for="medium" >Quantity</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-1">
                            <h3><span class="badge blue">L</span></h3>
                        </div>
                        <div class="col-md-3">
                            <div class="md-form">
                                <input type="text" id="large" name="large" class="form-control">
                                <label class="active" for="large" >Quantity</label>
                            </div>
                        </div>
                    </div>
                </div>
         

                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-2">
                            <h3><span class="badge blue">XL</span></h3>
                        </div>
                        <div class="col-md-3">
                            <div class="md-form">
                                <input type="number" id="xl" name="xl" class="form-control">
                                <label class="active" for="xl"  >Quantity</label>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-2">
                            <h3><span class="badge blue">XXL</span></h3>
                        </div>
                        <div class="col-md-3">
                            <div class="md-form">
                                <input type="number" id="xxl" name="xxl" class="form-control">
                                <label class="active" for="xxl" >Quantity</label>
                            </div>
                        </div>
                    </div>
                
                    <div class="row">
                        <div class="col-md-2">
                            <h3><span class="badge blue">XXXL</span></h3>
                        </div>
                        <div class="col-md-3">
                            <div class="md-form">
                                <input type="number" id="xxxl" name="xxxl" class="form-control">
                                <label class="active" for="xxxl">Quantity</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div><br>
            <button type="submit" class="btn btn-primary">Ask For Quote</button>
        </div>
    </form> 


  </div>
</div>       
@endsection