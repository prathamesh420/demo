@extends('layouts.app')

@section('content')


<!--Carousel Wrapper-->
<div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
    <!--Indicators-->
    <ol class="carousel-indicators">
        <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
        <li data-target="#carousel-example-1z" data-slide-to="1"></li>
        <li data-target="#carousel-example-1z" data-slide-to="2"></li>
    </ol>
    <!--/.Indicators-->
    <!--Slides-->
    <div class="carousel-inner" role="listbox"  style="max-height:400px !important;">
        <!--First slide-->
        <div class="carousel-item active">
            <div class="view">
                <img class="d-block w-100" src="images/event.jpg" alt="First slide">
                <div class="mask rgba-black-light"></div>
            </div>
            <div class="carousel-caption">
                <h2 class="h2-responsive">Event Tshirts</h2>
            </div>
        </div>   
        <!--/First slide-->

        <!--Second slide-->
        <div class="carousel-item">
            <div class="view">
                <img class="d-block w-100" src="images/hospital.jpg" alt="Second slide">
                <div class="mask rgba-black-light"></div>
            </div>
            <div class="carousel-caption">
                    <h2 class="h2-responsive">Hospitality Industry</h2>
            </div>
        </div>
        <!--/Second slide-->

        <!--Third slide-->
        <div class="carousel-item">
            <div class="view">
                <img class="d-block w-100" src="images/3.jpg" alt="Third slide">
                <div class="mask rgba-black-light"></div>
            </div>
            <div class="carousel-caption">
                <h2 class="h2-responsive">Hiviz Tshirts</h2>
            </div>
        </div>
        <!--/Third slide-->
    </div>
    <!--/.Slides-->
    <!--Controls-->
    <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    <!--/.Controls-->
</div>
<!--/.Carousel Wrapper-->

<div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="text-center page-title">Tshirts for Events</h1>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <div class="product-container">
          <div class="product-image">
            <span class="hover-link"></span>
            <a href="#" class="product-link">Ask for Quote</a>
            <img class="img-responsive" src="images/tshirt1.jpg" alt="">
          </div>
          <div class="product-description">
            <div class="product-label">
              <div class="product-name">
                <h1>Round Neck Half-Sleeve</h1>
                <p class="price"></p>
                <p></p>
              </div>
            </div>
            <div class="product-option">
              <div class="product-size">
                <h3>Sizes</h3>
                <p>S,M,L,XL,XXL,XXXL</p>
              </div>
              <div class="product-color">
                <h3>Colors</h3>
                <ul>
                  <li class="red"></li>
                  <li class="blue"></li>
                  <li class="green"></li>
                  <li class="gray"></li>
                  <li class="black"></li>
                  <li class="dark-blue"></li>
                </ul>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="product-container">
          <div class="product-image">
            <span class="hover-link"></span>
            <a href="#" class="product-link">Ask For Quote</a>
            <img class="img-responsive" src="images/tshirt6.jpg" alt="">
          </div>
          <div class="product-description">
            <div class="product-label">
              <div class="product-name">
                <h1>Round Neck Full-Sleeve </h1>
                <p class="price"></p>
                <p></p>
              </div>
            </div>
            <div class="product-option">
              <div class="product-size">
                <h3>Sizes</h3>
                <p>S,M,L,XL,XXL,XXXL</p>
              </div>
              <div class="product-color">
                <h3>Colors</h3>
                <ul>
                  <li class="red"></li>
                  <li class="blue"></li>
                  <li class="green"></li>
                  <li class="gray"></li>
                  <li class="black"></li>
                  <li class="dark-blue"></li>
                </ul>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="product-container">
          <div class="product-image">
            <span class="hover-link"></span>
            <a href="#" class="product-link">Ask For Quote</a>
            <img class="img-responsive" src="images/tshirt2.jpg" alt="">
          </div>
          <div class="product-description">
            <div class="product-label">
              <div class="product-name">
                <h1>V-Neck Full Sleeve Tshirt</h1>
                <p class="price"></p>
                <p></p>
              </div>
            </div>
            <div class="product-option">
              <div class="product-size">
                <h3>Sizes</h3>
                <p>S,M,L,XL,XXL,XXXL</p>
              </div>
              <div class="product-color">
                <h3>Colors</h3>
                <ul>
                  <li class="red"></li>
                  <li class="blue"></li>
                  <li class="green"></li>
                  <li class="gray"></li>
                  <li class="black"></li>
                  <li class="dark-blue"></li>
                </ul>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="product-container">
          <div class="product-image">
            <span class="hover-link"></span>
            <a href="#" class="product-link">Ask For Quote</a>
            <img class="img-responsive" src="images/tshirt7.jpg" alt="">
          </div>
          <div class="product-description">
            <div class="product-label">
              <div class="product-name">
                <h1>V-Neck Full Sleeve Tshirt</h1>
                <p class="price"></p>
                <p></p>
              </div>
            </div>
            <div class="product-option">
              <div class="product-size">
                <h3>Sizes</h3>
                <p>S,M,L,XL,XXL,XXXL</p>
              </div>
              <div class="product-color">
                <h3>Colors</h3>
                <ul>
                  <li class="red"></li>
                  <li class="blue"></li>
                  <li class="gray"></li>
                  <li class="black"></li>
                  <li class="dark-blue"></li>
                </ul>
              </div>

            </div>
          </div>
        </div>
        
      </div>
      
    </div>
    <div class="row">
      <div class="col-md-3">
        <div class="product-container">
          <div class="product-image">
            <span class="hover-link"></span>
            <a href="#" class="product-link">Ask for Quote</a>
            <img class="img-responsive" src="images/1.jpg" alt="">
          </div>
          <div class="product-description">
            <div class="product-label">
              <div class="product-name">
                <h1>Collar Neck Half-Sleeve</h1>
                <p class="price"></p>
                <p></p>
              </div>
            </div>
            <div class="product-option">
              <div class="product-size">
                <h3>Sizes</h3>
                <p>S,M,L,XL,XXL,XXXL</p>
              </div>
              <div class="product-color">
                <h3>Colors</h3>
                <ul>
                  <li class="red"></li>
                  <li class="blue"></li>
                  <li class="gray"></li>
                  <li class="black"></li>
                  <li class="dark-blue"></li>
                </ul>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="product-container">
          <div class="product-image">
            <span class="hover-link"></span>
            <a href="#" class="product-link">Ask For Quote</a>
            <img class="img-responsive" src="images/tshirt8.jpg" alt="">
          </div>
          <div class="product-description">
            <div class="product-label">
              <div class="product-name">
                <h1>Collar Neck Full-Sleeve Tshirt</h1>
                <p class="price"></p>
                <p></p>
              </div>
            </div>
            <div class="product-option">
              <div class="product-size">
                <h3>Sizes</h3>
                <p>S,M,L,XL,XXL,XXXL</p>
              </div>
              <div class="product-color">
                <h3>Colors</h3>
                <ul>
                  <li class="red"></li>
                  <li class="blue"></li>
                  <li class="green"></li>
                  <li class="gray"></li>
                  <li class="black"></li>
                  <li class="dark-blue"></li>
                </ul>
              </div>

            </div>
          </div>
        </div>
      </div>
      </div>

       <div class="row">
        <div class="col-md-12">
          <h1 class="text-center page-title">Uniforms for Hospital Industry & Food Franchises</h1>
        </div>
     </div>
     <div class="row">
      <div class="col-md-3">
        <div class="product-container">
          <div class="product-image">
            <span class="hover-link"></span>
            <a href="#" class="product-link">Ask For Quote</a>
            <img class="img-responsive" src="images/uni2.jpg" alt="">
          </div>
          <div class="product-description">
            <div class="product-label">
              <div class="product-name">
                <h1>Medical Lab Coat</h1>
                <p class="price"></p>
                <p></p>
              </div>
            </div>
            <div class="product-option">
              <div class="product-size">
                <h3>Sizes</h3>
                <p>S,M,L,XL,XXL,XXXL</p>
              </div>
              <div class="product-color">
                <h3>Colors</h3>
                <ul>
                  <li class="red"></li>
                  <li class="blue"></li>
                </ul>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="product-container">
          <div class="product-image">
            <span class="hover-link"></span>
            <a href="#" class="product-link">Ask For Quote</a>
            <img class="img-responsive" src="images/uni3.jpg" alt="">
          </div>
          <div class="product-description">
            <div class="product-label">
              <div class="product-name">
                <h1>White Chef Coat</h1>
                <p class="price"></p>
                <p></p>
              </div>
            </div>
            <div class="product-option">
              <div class="product-size">
                <h3>Sizes</h3>
                <p>S,M,L,XL,XXL,XXXL</p>
              </div>
              <div class="product-color">
                <h3>Colors</h3>
                <ul>
                  <li class="red"></li>
                  <li class="blue"></li>
                </ul>
              </div>

            </div>
          </div>
        </div>
        
      </div>
      
    </div>

      <div class="row">
        <div class="col-md-12">
          <h1 class="text-center page-title">Hiviz Tshirts & Worker Wear</h1>
        </div>
     </div>
     <div class="row">
      <div class="col-md-3">
        <div class="product-container">
          <div class="product-image">
            <span class="hover-link"></span>
            <a href="#" class="product-link">Ask For Quote</a>
            <img class="img-responsive" src="images/tshirt5.jpg" alt="">
          </div>
          <div class="product-description">
            <div class="product-label">
              <div class="product-name">
                <h1>Hiviz Half-Sleeve Tshirt</h1>
                <p class="price"></p>
                <p></p>
              </div>
            </div>
            <div class="product-option">
              <div class="product-size">
                <h3>Sizes</h3>
                <p>S,M,L,XL,XXL,XXXL</p>
              </div>
              <div class="product-color">
                <h3>Colors</h3>
                <ul>
                  <li class="red"></li>
                  <li class="blue"></li>
                </ul>
              </div>

            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
        <div class="product-container">
          <div class="product-image">
            <span class="hover-link"></span>
            <a href="#" class="product-link">Ask For Quote</a>
            <img class="img-responsive" src="images/tshirt4.jpg" alt="">
          </div>
          <div class="product-description">
            <div class="product-label">
              <div class="product-name">
                <h1>Hiviz Full-Sleeve Tshirt</h1>
                <p class="price"></p>
                <p></p>
              </div>
            </div>
            <div class="product-option">
              <div class="product-size">
                <h3>Sizes</h3>
                <p>S,M,L,XL,XXL,XXXL</p>
              </div>
              <div class="product-color">
                <h3>Colors</h3>
                <ul>
                  <li class="red"></li>
                  <li class="blue"></li>
                </ul>
              </div>

            </div>
          </div>
        </div>
        
      </div>
      
    </div>
  </div>



@endsection