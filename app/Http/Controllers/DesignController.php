<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Design;



class DesignController extends Controller
{
    public function insert(Request $request){
        $design = new Design();
        $design->pattern = $request->input('pattern');
        $design->sleeve = $request->input('sleeve');
        $design->logoPlacement = $request->input('logoPlacement');
        $design->text = $request->input('text');
        $design->textPlacement = $request->input('textPlacement');
        $design->color = $request->input('color');
        $design->small = $request->input('small');
        $design->medium = $request->input('medium');
        $design->large = $request->input('large');
        $design->xl = $request->input('xl');
        $design->xxl = $request->input('xxl');
        $design->xxxl = $request->input('xxxl');

        $request->designs()->save($design);
        return redirect('/design')->with('Success','Quotation Sent');
    }

}